//
//  FWCoordinator.swift
//  Drinking Games
//
//  Created by cain on 5/3/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

class FWCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {
    
    weak var parent_Coordinator:InitialCoordinator?
    var navigation_controller: UINavigationController
    
    var child_coordinators = [Coordinator]()
    
    init(nav_controller: UINavigationController) {
        self.navigation_controller = nav_controller
        
    }
    
    func start() {
        navigation_controller.delegate = self
        let vc                         = FWGamePlayVC()
        
        vc.setCoordinator(self)
        
        navigation_controller.pushViewController(vc, animated: true)
    }
    
    
}
