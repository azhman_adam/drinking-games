//
//  IniitalCoordiantor.swift
//  Drinking Games
//
//  Created by cain on 3/21/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class InitialCoordinator: NSObject, Coordinator, UINavigationControllerDelegate{
    
    //MARK: - Vars
    var navigation_controller: UINavigationController
    var child_coordinators = [Coordinator]()
    
    
    //MARK: - Intializers
    init(nav_controller: UINavigationController) {
        navigation_controller                        = nav_controller
        navigation_controller.navigationBar.isHidden = true
    }
    
    
    
    //MARK: - Coordinator Functions
    func start() {
        let vc = InitialVC()
        vc.coordinator = self
        
        navigation_controller.pushViewController(vc, animated: true)
        
        
    }
    
    func playNeverHaveIEver(){
        let child                = NHIECoordinator(nav_controller: navigation_controller)
        child.parent_coordinator = self
        
        child_coordinators.append(child)
        child.start()
    }
    
    func playLuckyBottle(){
        let child                  = LBCoordinator(nav_controller: navigation_controller)
        child.parrent_coordinatotr = self
        
        child_coordinators.append(child)
        
        child.start()
    }
    
    func playFortuneWheel(){
        let child                = FWCoordinator(nav_controller: navigation_controller)
        child.parent_Coordinator = self
        
        child_coordinators.append(child)
        
        child.start()
        
    }
    
    
    func childDidFinish(_ child: Coordinator){
        
        for (index, coordinator) in child_coordinators.enumerated(){
            if child === coordinator{
                child_coordinators.remove(at: index)
                return
            }
        }
    }
}
