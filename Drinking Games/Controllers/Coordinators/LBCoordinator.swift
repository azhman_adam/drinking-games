//
//  LBCoordinator.swift
//  Drinking Games
//
//  Created by cain on 5/2/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class LBCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {
  
    //MARK: - Vars
    weak var parrent_coordinatotr: InitialCoordinator?
    var navigation_controller: UINavigationController
    var child_coordinators = [Coordinator]()
    
    init(nav_controller: UINavigationController) {
        
        self.navigation_controller = nav_controller
        
    }
    
    func start() {
        
        navigation_controller.delegate = self
        let vc = LBGamePlayVC()
        
        vc.setCoordinator(self)
        
        navigation_controller.pushViewController(vc, animated: true)
    }
    
    
}
