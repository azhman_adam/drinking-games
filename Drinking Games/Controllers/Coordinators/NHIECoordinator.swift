//
//  NHIECoordinator.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class NHIECoordinator: NSObject, Coordinator, UINavigationControllerDelegate{
    
    //MARK: - Vars
    weak var parent_coordinator: InitialCoordinator?
    var navigation_controller: UINavigationController
    var child_coordinators = [Coordinator]()
    
    //MARK: - Initializers
    init(nav_controller: UINavigationController){
        navigation_controller = nav_controller
      
    }
    
    //MARK: - CoordinatorFunctions
    func start() {
        
        navigation_controller.delegate = self
        let vc                         = NHIEIntroVC()
        vc.coordinator                 = self
        
        navigation_controller.pushViewController(vc, animated: true)
    }
    
    func goToGameSelection(players: [String]){
        let vc = NHIEGameSelectionVC()
        
        vc.coordinator(self)
        vc.player(players)
        
        navigation_controller.pushViewController(vc, animated: true)
        
    }
    
    func backToNHIEFirstPage(){
        
        navigation_controller.popViewController(animated: true)
        
    }
    
    func goToPlayNHIE(players: [String], game_mode: GameMode){
        
        let vc = NHIEGamePlayVC()
        
        vc.coordinator(self)
        vc.players(players)
        vc.gameMode(game_mode)
        
        
        navigation_controller.pushViewController(vc, animated: true)
    }
    
    func didFinish(){
        parent_coordinator?.childDidFinish(self)
    }
    
}
