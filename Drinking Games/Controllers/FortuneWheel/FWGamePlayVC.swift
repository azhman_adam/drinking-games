//
//  FWGamePlayVC.swift
//  Drinking Games
//
//  Created by cain on 5/3/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class FWGamePlayVC: UIViewController {

    //MARK: - Vars
    private weak var _coordinator: FWCoordinator?
    
    private var _prompt_view: FWGamePlayV?
 
    
    //MARK: - Getter, Setter
    func setCoordinator(_ coordinator: FWCoordinator){
        
        self._coordinator = coordinator
    }
    
    //MARK: - VCLifeCycle
    override func loadView() {
        _prompt_view = FWGamePlayV()
        
        view = _prompt_view
        
        _prompt_view?.setVC(self)
    }
    
    override func viewDidLayoutSubviews() {
        
        _prompt_view?.postConfigurations()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
    }
    
    //MARK: - UserInteractionss
    
    @objc func playTheGame(_ sender: UIButton){
        //slices - 1
        let random = Int.random(in: 0...3)
        
        _prompt_view?.fortune_wheel.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self._prompt_view?.fortune_wheel.startAnimating(fininshIndex: random) { (finished) in
                print(finished)
                
                print("selected one is \(self._prompt_view!.slices[random].title )")
                

            }
        }
        
    }


}
