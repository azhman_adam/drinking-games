//
//  InitialVC.swift
//  Drinking Games
//
//  Created by cain on 3/21/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imprts
import UIKit

class InitialVC: UIViewController {

    //MARK: - Vars
    weak var coordinator: InitialCoordinator?
    private var promt_view = InitialV()
    
    
    //MARK: - VCLifeCyle
    override func loadView() {
        promt_view.vc = self
        view = promt_view
        
    }
    
    override func viewDidLoad() {
        
    }
    
    override func viewDidLayoutSubviews() {
        promt_view.postConfigurations()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        Alert.initialWarning(on: self)
    }
    //MARK: - UserInteractions
    @objc func goToNevermind(_ sender: UIButton){
        coordinator?.playNeverHaveIEver()
    }
    
    @objc func goToPurchase(_ sender: UIButton){
        print("purchased is initialized")
    } //TODO: - Completing the action
    
    @objc func langSelectionMode(_ sender: UIButton){
        print("lanselection mode is activated")
    } //TODO: - Completing the action
    
    @objc func goToFortunewheel(_ sender: UIButton){
        coordinator?.playFortuneWheel()
    }
    
    @objc func goToLuckybottle(_ sender: UIButton){
        coordinator?.playLuckyBottle()

    }
    
    @objc func goToTaproulette(_ sender: UIButton){
        print("tap roulette")
    }
}
