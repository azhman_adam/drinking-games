//
//  LBGamePlayVC.swift
//  Drinking Games
//
//  Created by cain on 4/28/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class LBGamePlayVC: UIViewController {

    //MARK: - Vars
    private weak var _coordinator: LBCoordinator?
    private var _prompt_View: LBGamePlayV?
    private var _num = 0
    
    //MARK: - Setter&Getter
    func setCoordinator(_ coordinator: LBCoordinator){
        self._coordinator = coordinator
    }
    
    //MARK: - VCLifeCycle
    override func loadView() {
        _prompt_View = LBGamePlayV()
        _prompt_View?.setVC(self)
        
        view = _prompt_View
        
        
    }
    
    override func viewDidLayoutSubviews() {
        
 
        
        _prompt_View!.postConfigurations()
        
    }
    
    //MARK: - UserIntractions
    @objc func playTheGame(_ sender: UIButton){
        
        self.rotate2(imageView: _prompt_View!.bottle_image, aCircleTime: 0.5)
        let image = _prompt_View!.bottle_image.image?.fixedOrientation()
        _prompt_View!.bottle_image.image = image
        
    }
    
    func rotate2(imageView: UIImageView, aCircleTime: Double , shouldFinish : Bool = false) { //UIView
        
        if shouldFinish{
            // print("hi")
            self._num = 0
            
            UIView.animate(withDuration: aCircleTime, delay: 0.0, options: [.curveEaseOut ], animations: {
                let random = Float.random(in: 0...2)
                //print(random * .pi)
                imageView.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * random))
            }, completion: { finished in
                // print("finished")
            })
        }else{
            UIView.animate(withDuration: aCircleTime/2, delay: 0.0, options: [.curveLinear], animations: {
                imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            }, completion: { finished in
                UIView.animate(withDuration: aCircleTime/2, delay: 0.0, options: [.curveLinear], animations: {
                    imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi*2))
                }, completion: { finished in
                    if self._num == 10{
                        self.rotate2(imageView: imageView, aCircleTime: aCircleTime  , shouldFinish: true)
                    }else{
                        self._num += 1
                        self.rotate2(imageView: imageView, aCircleTime: aCircleTime  , shouldFinish: false)
                    }
                    
                    
                })
            })
        }
        
    }
    
    func rotate3(imageView: UIImageView, aCircleTime: Double) { //CAKeyframeAnimation
        
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        animation.duration = aCircleTime
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.repeatCount = .infinity
        animation.values = [0, Double.pi/2, Double.pi, Double.pi*3/2, Double.pi*2]
        
        //Percentage of each key frame
        //animation.keyTimes = [NSNumber(value: 0.0), NSNumber(value: 0.3),
        //NSNumber(value: 0.6), NSNumber(value: 0.9), NSNumber(value: 1.0)]
        imageView.layer.add(animation, forKey: "rotate")
    }
}








