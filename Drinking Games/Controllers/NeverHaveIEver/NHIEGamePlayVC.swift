//
//  NHIEGamePlayVC.swift
//  Drinking Games
//
//  Created by cain on 4/4/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MAKR: - Imports
import UIKit

class NHIEGamePlayVC: UIViewController {

    //MARK: - Vars
    private weak var _coordinator: NHIECoordinator?
    private var _players: [String]!
    private var _question_maneger:QuestionManager!
    private var _game_mode: GameMode!{
           didSet{
               questionsExtraction()
           }
       }
    
    private var _prompt_view        = NHIEGamePlayV()
    private var _questionRound1     = [Question]()
    private var _questionRound2     = [Question]()
    private var _questionRound3     = [Question]()
    private var _language: Language = .english
    
    private var _round: Round     = .one
    private var _question_counter = 0
    
    
    
    
    
    
    //MARK: - Getter,Setter
    func coordinator(_ object: NHIECoordinator){
        self._coordinator = object
    }
    
    func players(_ object: [String]){
         self._players = object
     }
    
    func gameMode(_ object: GameMode){
        
        self._game_mode = object
        
    }
    
    //MARK: - Configurations
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeLeft
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    //MARK: - VCLifeCyle
    override func loadView() {
        view = _prompt_view
        _prompt_view.setViewController(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")

       
    }
    
    override func viewDidLayoutSubviews() {
        
        _prompt_view.postConfigurations()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _prompt_view.setQuestion(text: _questionRound1[_question_counter].text, first: true)
        _question_counter += 1
        _prompt_view.startTheRound(number: .one, show_state: false)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        _prompt_view.startTheRound(number: .one, show_state: true)
        
    }
    
    
    //MARK: - UserInteractionss
    @objc func endButtonTouchUpInside(_ sender: UIButton){
        
       
    }
    
    @objc func nextQuestionIsRequested(_ sender: UITapGestureRecognizer){
  
        setQuestion()
        
    }
    
    
    //MARK: - Configurations
    
    private func questionsExtraction(){
        _question_maneger = QuestionManager(with: _language, for: _game_mode)
        
        _questionRound1 = _question_maneger.questionRound1()
        _questionRound2 = _question_maneger.questionRound2()
        _questionRound3 = _question_maneger.questionRound3()
        
        print(_questionRound1.count)
        print(_questionRound1[0].text)
    }
    
    private func setQuestion(){
        
        switch _round {
            case .one:
                
                if _question_counter < 10 {
                    
                    let text = _questionRound1[_question_counter].text
                    
                    _prompt_view.setQuestion(text: text, first: false)
                    
                    _question_counter += 1
                    
                }else if _question_counter == 10{
                    _round            = .two
                    _question_counter = 0
                    
                    _prompt_view.setQuestion(text: _questionRound2[_question_counter].text, first: true)
                    _prompt_view.startTheRound(number: .two, show_state: false)
                    

                    
                    fallthrough
                    
            }
            
            
            case .two:
                if _question_counter == 0 {
                    _question_counter += 1
                    _prompt_view.startTheRound(number: .two, show_state: true)
                    
                }else if _question_counter < 10 {
                    _prompt_view.setQuestion(text: _questionRound2[_question_counter].text, first: false)
                    _question_counter += 1
                    
                }else if _question_counter == 10{
                    _round = .three
                    _question_counter = 0
                    
                    _prompt_view.setQuestion(text: _questionRound3[_question_counter].text, first: true)
                    _prompt_view.startTheRound(number: .three, show_state: false)
                    
                    fallthrough
            }
                
                
            
            case .three:
                if _question_counter == 0 {
                    _question_counter += 1
                    _prompt_view.startTheRound(number: .three, show_state: true)
                    
                }else if _question_counter < 10{
                    _prompt_view.setQuestion(text: _questionRound3[_question_counter].text, first: false)
                    _question_counter += 1
                    
                    
                }else if _question_counter == 10 {
                    //finish the game
            }
            
        }
        
    }
    
    
    
    
    
}
