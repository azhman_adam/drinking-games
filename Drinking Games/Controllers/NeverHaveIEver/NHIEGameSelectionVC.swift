//
//  NHIEGameSelectionVC.swift
//  Drinking Games
//
//  Created by cain on 3/28/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class NHIEGameSelectionVC: UIViewController {

    //MARK: - Vars
    private weak var _coordinator: NHIECoordinator?
    private var _players: [String]!
    private var _promt_view               = NHIEGameSelectionV()
    private var _last_game_mode: GameMode = .nope
   
    
    //MARK: Getter,Setter
    func player(_ object: [String]){
        self._players = object
    }
    
    func coordinator(_ object: NHIECoordinator){
        self._coordinator = object
    }
    
    //MARK: - VCLifeCycle
    override func loadView() {
        _promt_view.vc = self
        view = _promt_view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

            
        
    }
    
    override func viewDidLayoutSubviews() {
        
        _promt_view.postConfiguration()
        
    }
    
    
    //MARK: - UserInteraction
    @objc func nopeTouchUPInside(_ sender: UIButton){
        
        gameModeIsChoosed(.nope)
        
        
    }
    
    @objc func customTouchUPInside(_ sender: UIButton){
        
        gameModeIsChoosed(.custom)
        
        
    }
    
    @objc func sillyTouchUPInside(_ sender: UIButton){
        
        gameModeIsChoosed(.silly)
        
        
    }
    
    @objc func hotTouchUPInside(_ sender: UIButton){
        
        gameModeIsChoosed(.hot)
        
        
    }
    
    @objc func springbreakTouchUPInside(_ sender: UIButton){
        
        gameModeIsChoosed(.springbreak)
        
        
    }
    
    @objc func backTouchUpInside(_ sender: UIButton){
        
        _coordinator?.backToNHIEFirstPage()
        
        
    }
    
    @objc func goTouchUpInside(_ sender: UIButton){
        
        _coordinator?.goToPlayNHIE(players: _players, game_mode: _last_game_mode)
        
    }


    //MARK: - Configurations
    private func gameModeIsChoosed(_ mode: GameMode){
        
        
        _promt_view.changeStyleToDark(selected_game_mode: _last_game_mode)
        _last_game_mode = mode
        _promt_view.changeStyleToLight(selected_game_mode: mode)
        
    }
}
