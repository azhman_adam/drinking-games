//
//  NHIEIntroVC.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class NHIEIntroVC: UIViewController {

    //MARK: - Vars
    weak var coordinator: NHIECoordinator?
    var promt_view      = NHIEIntroV()
    var delegate        = PlayerTVDelegate()
    var datasource      = PlayerTVDataSource()
    var player_manager  = PlayerManager()

    //MARK: - VCLifeCyle
    override func loadView() {
        promt_view.vc             = self
        datasource.player_manager = player_manager
        datasource.vc             = self
        view                      = promt_view
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        promt_view.setPlayerTVDelegateDataSource(delegate, datasource)
        
       
    }
    
    override func viewDidLayoutSubviews() {
        promt_view.postConfigurations()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    //MARK: - UserInteractions
    @objc func addMoreCell(_ sender: UIButton){
        datasource.number_of_players += 1
        promt_view.playerTVReloadData()

        promt_view.playerTVScrollBottom()
    }
    
    @objc func playerTextFieldDidEditting(_ sender: UITextField){

        if sender.text != ""{
           
            player_manager.addPlayer(with_name: sender.text!, at_row: sender.tag)
        }
        
    }
    
    @objc func startButtonPressed(_ sender: UIButton){
        let number_of_players = player_manager.players.count
        
        if number_of_players < 2 {
            
            Alert.enterMorehPlayers(on: self)
        }else{
            
            var name = [String]()
            
            for item in player_manager.players{
                name.append(item.value.name)
            }
            
            if name.count != Set(name).count{
                
                Alert.playersNameShouldBeUnique(on: self)
                
            }else{
                
                coordinator?.goToGameSelection(players: name)
                
            }
            
            
        }
        
        
        
        
        
    }
}
