//
//  AlertVC.swift
//  Drinking Games
//
//  Created by cain on 3/29/20.
//  Copyright © 2020 cain. All rights reserved.
//
//
//MARK: - Imports
import UIKit

class AlertVC: UIViewController {
    
    //MARK: - Vars
    weak var coordinator: Coordinator?
    private var prompt_view = AlertV()
    
    
    //MArk: - VCLifeCycle
    override func loadView() {
        
        view = prompt_view
        prompt_view.vc = self
    }
    
    override func viewDidLoad() {
        
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    //MARK: - UserIntractions
    @objc func actionIsPressed(_ sender: UIButton){
        NSLog("action accepted")
    }
    
    @objc func cancelIsPressed(_ sender: UIButton){
        NSLog("action refused")
    }
    
    
    //MARK: - Configurations


}
