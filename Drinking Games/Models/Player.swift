//
//  Player.swift
//  Drinking Games
//
//  Created by cain on 3/25/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import Foundation

class Player{
    
     var name: String
    
    init(name: String){
        self.name = name
    }
    
}
