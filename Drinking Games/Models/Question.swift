//
//  Question.swift
//  Drinking Games
//
//  Created by cain on 4/4/20.
//  Copyright © 2020 cain. All rights reserved.
//

import Foundation

struct Question: Equatable {
    
    //MARK: - Vars
    private var _key: String
    private var _parent_key: String
    private var _text: String
    private var _type: Int
    private var _cycle_state: Bool
    private var _pack_name: String
    private var _language: String
    
    
    
    //MARK: - Initalizer
    
    init(key: String, parent_key: String, text: String, type: Int, cycle_state: Bool, pack_name: String, language: String) {
        
        self._key         = key
        self._parent_key  = parent_key
        self._text        = text
        self._type        = type
        self._cycle_state = cycle_state
        self._pack_name   = parent_key
        self._language    = language
    }
    
    
    //MARK: - Getter
    var key: String{
        get{
            return _key
        }
    }
    var parent_key: String{
        get{
            return _parent_key
        }
    }
    var text: String{
        get{
            return _text
        }
    }
    var type: Int{
        get{
            return _type
        }
    }
    var cycle_state: Bool{
        get{
            return _cycle_state
        }
    }
    var pack_name: String{
        get{
            return _pack_name
        }
    }
    var lnaguage: String{
        get{
            return _language
        }
    }
    
    //MARK: - Conform to protocol
    static func == (lhs: Question, rhs: Question) -> Bool {
        return lhs._key == rhs._key && lhs._parent_key == rhs._parent_key && lhs._text == rhs._text && lhs._type == rhs._type && lhs._cycle_state == rhs._cycle_state && lhs._pack_name == rhs._pack_name && lhs._language == rhs._language
    }
}
