//
//  QuestionManager.swift
//  Drinking Games
//
//  Created by cain on 4/4/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import Foundation
import RealmSwift

struct QuestionManager {
    
    //Mark: - Vars
    private let realm: Realm
    private var _rule_results: Results<Rule>?
    private var _questions: [Question]?
    private var _questions_round1 = [Question]()
    private var _questions_round2 = [Question]()
    private var _questions_round3 = [Question]()
    
    private var language: Language
    private var pack_name: String
    
    
    func questionRound1() -> [Question]{
        return _questions_round1
    }
    func questionRound2() -> [Question]{
        return _questions_round2
    }
    func questionRound3() -> [Question]{
        return _questions_round3
    }
    
    //MAKR: - Initializer
    init(with language: Language, for mode: GameMode) {
        self.realm         = try! Realm()
        
        self.language = language
        if mode != .nope {
            self.pack_name = mode.rawValue
        }else{
            self.pack_name = "default"
        }
        
        extractingQuestions()
    }
    
    
    private static func initializingDataBase(){
        
        do{
            
            _ = try Realm()
            
        }catch let error{
            
            NSLog("\(error)")
            
        }
        
    }
    
    static func initialDBConfigurations(){
        
        let realm_db_path     = Realm.Configuration.defaultConfiguration.fileURL!
        let bundle_realm_path = Bundle.main.url(forResource: "default", withExtension: "realm")!
        
        if !FileManager.default.fileExists(atPath: realm_db_path.absoluteString){
            do {
                
                try FileManager.default.copyItem(at: bundle_realm_path, to: realm_db_path)
                
            }catch let error {
                
                NSLog("\(error)")
                
            }
        }
        
        initializingDataBase()
        
    }
    
    private mutating func extractingQuestions()  {
        
        _rule_results = realm.objects(Rule.self).filter("language = '\(language.rawValue)' and pack_name = '\(pack_name)'")
        
        
        
        extractQuestionsForRound1(from: _rule_results!)
        extractQuestionsForRound2(from: _rule_results!)
        extractQuestionsForRound3(from: _rule_results!)
        
        
        
        
    }
    
    private mutating func extractQuestionsForRound1(from questions: Results<Rule>) {
        
        var c = 0
        let shuffledquestion = questions.shuffled()
        while _questions_round1.count < 10 {
            
            let question = Question(key: shuffledquestion[c].key, parent_key: shuffledquestion[c].parent_key, text: shuffledquestion[c].text, type: shuffledquestion[c].type, cycle_state: shuffledquestion[c].cycle_state, pack_name: shuffledquestion[c].pack_name, language: shuffledquestion[c].language)
            
            if question.parent_key != "" && _questions_round1.count < 9 {
                
                if extractParentQuestion(from: questions, for: shuffledquestion[c], on: 1){
                    _questions_round1.append(question)
                }
                
            }else{
                
                _questions_round1.append(question)
                
                if question.key != "" && _questions_round1.count < 10{
                    extractChildQuestion(from: questions, for: shuffledquestion[c], on: 1)
                }
            }
            c += 1
        }
    }
    
    private mutating func extractQuestionsForRound2(from questions: Results<Rule>){
        
        var c = 0
        let shuffledquestion = questions.shuffled()
        while _questions_round2.count < 10 {
            
            let question = Question(key: shuffledquestion[c].key, parent_key: shuffledquestion[c].parent_key, text: shuffledquestion[c].text, type: shuffledquestion[c].type, cycle_state: shuffledquestion[c].cycle_state, pack_name: shuffledquestion[c].pack_name, language: shuffledquestion[c].language)
            
            if _questions_round1.contains(question){
                c += 1
                continue
            }
            
            if question.parent_key != "" && _questions_round2.count < 9{
                
                if extractParentQuestion(from: questions, for: shuffledquestion[c], on: 2){
                    _questions_round2.append(question)
                }
                
            }else{
                
                _questions_round2.append(question)
                
                if question.key != "" && _questions_round2.count < 10{
                    extractChildQuestion(from: questions, for: shuffledquestion[c], on: 2)
                }
            }
            c += 1
        }
        
    }
    
    private mutating func extractQuestionsForRound3(from questions: Results<Rule>){
        
        var c = 0
         let shuffledquestion = questions.shuffled()
        while _questions_round3.count < 10 {
            
            let question = Question(key: shuffledquestion[c].key, parent_key: shuffledquestion[c].parent_key, text: shuffledquestion[c].text, type: shuffledquestion[c].type, cycle_state: shuffledquestion[c].cycle_state, pack_name: shuffledquestion[c].pack_name, language: shuffledquestion[c].language)
            
            if _questions_round1.contains(question) || _questions_round2.contains(question){
                c += 1
                continue
            }
            
            if question.parent_key != "" && _questions_round3.count < 9{
                
                if extractParentQuestion(from: questions, for: shuffledquestion[c], on: 3){
                    _questions_round3.append(question)
                }
                
            }else{
                
                _questions_round3.append(question)
                
                if question.key != "" && _questions_round3.count < 10{
                    extractChildQuestion(from: questions, for: shuffledquestion[c], on: 3)
                }
            }
            c += 1
        }
        
    }
    
    private mutating func extractChildQuestion(from questions: Results<Rule>, for question: Rule, on round: Int){
        let rules_child          = questions.filter("parent_key = '\(question.key)'")
        let shuffled_rules_child = rules_child.shuffled()
        
        
        for counter in 0..<shuffled_rules_child.count{
            
            let selected_child = shuffled_rules_child[counter]
            
            let question = Question(key: selected_child.key, parent_key: selected_child.parent_key, text: selected_child.text, type: selected_child.type, cycle_state: selected_child.cycle_state, pack_name: selected_child.pack_name, language: selected_child.language)
            
            switch round {
                case 1:
                    if !_questions_round1.contains(question) {
                        _questions_round1.append(question)
                }
                
                case 2:
                    if !_questions_round2.contains(question) && !_questions_round1.contains(question){
                        _questions_round2.append(question)
                }
                
                case 3:
                    if !_questions_round3.contains(question) &&
                        !_questions_round2.contains(question) &&
                        !_questions_round1.contains(question) {
                        
                        _questions_round3.append(question)
                        
                }
                default:
                    NSLog("Extracting child question for wrong round is requested!!")
            }
            
            
        }
        
        
    }
    
    private mutating func extractParentQuestion(from questions: Results<Rule>, for question: Rule, on round: Int) -> Bool{
        
        let rules_parent         = questions.filter("key = '\(question.parent_key)'")
        let shuffled_rules_child = rules_parent.shuffled()
        
        
        for counter in 0..<shuffled_rules_child.count{
            
            let selected_child = shuffled_rules_child[counter]
            
            let question = Question(key: selected_child.key, parent_key: selected_child.parent_key, text: selected_child.text, type: selected_child.type, cycle_state: selected_child.cycle_state, pack_name: selected_child.pack_name, language: selected_child.language)
            
            switch round {
                case 1:
                    if !_questions_round1.contains(question){
                        _questions_round1.append(question)
                        return true
                }
                
                case 2:
                    if !_questions_round2.contains(question) && !_questions_round1.contains(question){
                        _questions_round2.append(question)
                        return true
                }
                
                case 3:
                    if !_questions_round3.contains(question) &&
                        !_questions_round2.contains(question) &&
                        !_questions_round1.contains(question){
                        
                        _questions_round3.append(question)
                        return true
                        
                }
                
                default:
                    NSLog("Extracting parent question for wrong round is requested")
            }
            
            
        }
        
        return false
    }
    
    
    
    
    
    
    
    
    
    
}
