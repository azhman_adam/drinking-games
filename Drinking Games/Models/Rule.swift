import Foundation
import RealmSwift

class Rule: Object {
    @objc dynamic var key: String = ""
    @objc dynamic var parent_key: String = ""
    @objc dynamic var text: String = ""
    @objc dynamic var type: Int = 0
    @objc dynamic var cycle_state: Bool = false
    @objc dynamic var pack_name: String = ""
    @objc dynamic var language: String = ""
}

