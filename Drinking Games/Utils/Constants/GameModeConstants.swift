//
//  GameMode.swift
//  Drinking Games
//
//  Created by cain on 3/30/20.
//  Copyright © 2020 cain. All rights reserved.
//

import Foundation

let nope_title  = "Nope!"
let nope_image  = "nope_img"
let nope_deatil = "Use the wackiest \"Never have I ever\" as ice breakers. What an excellent way to get the party started!"

let custom_title  = "Custom"
let custom_image  = "custom_img"
let custom_detail = "Create your own rules and play fully-customized games. Let your devilish imagination run wild."

let silly_title  = "Silly"
let silly_image  = "silly_img"
let silly_detail = "Put all refinement and subtlety aside. This place is for total douches who want to get wasted at a party in full swing."

let hot_title  = "Hot"
let hot_image  = "hot_img"
let hot_detail = "Discover your friends' dirty little secrets with suggestive, sometimes even guilty \"Never have I ever\"s."

let springbreak_title  = "Spring Break"
let springbreak_image  = "springbreak_img"
let springbreak_detail = "Palm trees, beach volleyball, coconut trees, show-offs strutting their stuff on the beach... Relive your strangest vacation memories."
