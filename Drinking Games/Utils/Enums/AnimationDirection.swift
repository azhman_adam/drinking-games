//
//  AnimationDirection.swift
//  Drinking Games
//
//  Created by cain on 3/31/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

enum Direction: Int, CaseIterable{
    
    /// Direction of the animation used in AnimationType.from.
    case top
    case bottom
    case left
    case right
    
    /// Checks if the animation should go on the X or Y axis.
    var is_vertical: Bool{
        switch self {
            case .top, .bottom:
            return true
            
            case .right, .left:
            return false
        
        
        }
    }
    
     /// Positive or negative value to determine the direction.
    var sign: CGFloat{
        switch self {
            case .top, .left:
                return -1
            
            case .bottom, .right:
                return 1
        }
    }
    
    /// Random direction.
    static func random() -> Direction{
        
        return allCases.randomElement()!
    }
}
