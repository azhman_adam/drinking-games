//
//  AnimationTypes.swift
//  Drinking Games
//
//  Created by cain on 3/31/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

enum AnimationType: Animation{
    
    /// - from: Animation with direction and distance.
    /// - zoom: Zoom animation.
    /// - rotate: Rotation animation.
    case from(direction: Direction, offset: CGFloat)
    case zoom(scale: CGFloat)
    case rotate(angle: CGFloat)
    
    /// Creates the corresponding CGAffineTransform for AnimationType.from.
     var intial_transform: CGAffineTransform{
        switch self {
            
            case .from(let direction, let offset):
                let sign = direction.sign
            
                if direction.is_vertical {
                    
                    return CGAffineTransform(translationX: 0, y: offset * sign)
                    
                }else{
                    
                    return CGAffineTransform(translationX: offset * sign, y: 0)
                    
            }
            
            case .zoom(let scale):
                return CGAffineTransform(scaleX: scale, y: scale)
            
            case .rotate(let angle):
                return CGAffineTransform(rotationAngle: angle)
            
        }
    }
    
    /// Generates a random Animation.
    /// - Returns: Newly generated random Animation.
     static func random() -> Animation{
        
        let index = Int.random(in: 0..<3)
        
        if index ==  1{
            
            return AnimationType.from(direction: Direction.random(), offset: AnimationDefaultConfigues.offset)
            
        }else if index == 2{
            
            let scale = Double.random(in: 0...AnimationDefaultConfigues.max_zoom_scale)
            
            return AnimationType.zoom(scale: CGFloat(scale))
            
        }
        
        let angle = CGFloat.random(in: -AnimationDefaultConfigues.max_rotation_angle...AnimationDefaultConfigues.max_rotation_angle)
        
        return AnimationType.rotate(angle: angle)
        
        
        
        
        
        
        
    }
    
    
}
