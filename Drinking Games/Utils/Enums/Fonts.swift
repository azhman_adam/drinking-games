//
//  Fonts.swift
//  Drinking Games
//
//  Created by cain on 3/21/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

typealias rubik = Font.Rubik

enum Font{
    
    enum Rubik: String {
        case italic        = "Italic"
        case regular       = "Regular"
        case bold          = "Bold"
        case medium_italic = "MediumItalic"
        case medium        = "Medium"
        case black_italic  = "BlackItalic"
        case bold_italic   = "BoldItalic"
        case light_italic  = "LightItalic"
        case black         = "Black"
        case light         = "Light"
        
        func with(Size: CGFloat) -> UIFont {
            
            return UIFont(name: "Rubik-\(rawValue)", size: Size)!
        }
    }
}
