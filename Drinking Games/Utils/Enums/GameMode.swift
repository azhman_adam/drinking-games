//
//  GameMode.swift
//  Drinking Games
//
//  Created by cain on 3/30/20.
//  Copyright © 2020 cain. All rights reserved.
//

import Foundation

enum GameMode: String {
  
    case nope        = "nope"
    case custom      = "custom"
    case silly       = "silly"
    case hot         = "hot"
    case springbreak = "springbreak"
}
