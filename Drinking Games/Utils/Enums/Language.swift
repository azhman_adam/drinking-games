//
//  Language.swift
//  Drinking Games
//
//  Created by cain on 4/15/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import Foundation

enum Language: String{
    
    case english = "en"
    case french  = "fr"
    
}

