//
//  Round.swift
//  Drinking Games
//
//  Created by cain on 4/18/20.
//  Copyright © 2020 cain. All rights reserved.
//

import Foundation

enum Round: Int {
    case one = 1
    case two = 2
    case three = 3
}
