//
//  AnimationExtension.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

extension UIView {
    
    func pulsate(from_value: Float? = 0.9, to_value: Float? = 1.1) {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.4
        pulse.fromValue = from_value
        pulse.toValue = to_value
        pulse.autoreverses = true
        pulse.repeatCount = 100000
        pulse.initialVelocity = 0.5
        pulse.damping = 10
        
        layer.add(pulse, forKey: "pulse")
    }
    
    func flash(from_value: Float? = 1, to_value: Float? = 0.9) {
        
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.2
        flash.fromValue = from_value
        flash.toValue = to_value
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        flash.autoreverses = true
        flash.repeatCount = 100000
        
        layer.add(flash, forKey: nil)
    }
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.05
        shake.repeatCount = 200
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
    
}
