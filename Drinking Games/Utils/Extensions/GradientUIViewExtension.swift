//
//  GradientUIViewExtension.swift
//  Drinking Games
//
//  Created by cain on 4/28/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

extension UIView{
    
    func setLinearGradient(with colors: [CGColor], from start_point: CGPoint?, to end_point: CGPoint?){
        
        let gradient    = CAGradientLayer()
        gradient.frame  = self.bounds
        gradient.colors = colors
        
        if let startpoint = start_point, let endpoint = end_point{
            
            gradient.startPoint = startpoint
            gradient.endPoint   = endpoint
            
        }
        
        gradient.locations = [0.0 , 1.0]
        
        gradient.cornerRadius = self.layer.cornerRadius
        gradient.masksToBounds = true
        
        
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    
    
}
