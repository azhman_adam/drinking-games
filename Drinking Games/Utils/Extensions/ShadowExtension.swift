//
//  ShadowExtension.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

extension UIView {

    public func setShadow(with_color color: UIColor, opacity: Float, radius: CGFloat, offset_width width: Int, offset_height height: Int){
        self.layer.shadowColor   = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius  = radius
        self.layer.shadowOffset  = .init(width: width, height: height)
        
    }
    
    public func removeShadow(){
        
        self.layer.shadowColor   = UIColor.clear.cgColor
        self.layer.shadowOpacity = 0
        self.layer.shadowRadius  = 0
        self.layer.shadowOffset  = CGSize(width: 0, height: 0)
        
    }

}
