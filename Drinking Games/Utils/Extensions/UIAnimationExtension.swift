//
//  UIAnimationExtension.swift
//  Drinking Games
//
//  Created by cain on 3/31/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

extension UIView{
    
    /// - Parameters:
    ///   - animations: Array of Animations to perform on the animation block.
    ///   - reversed: Initial state of the animation. Reversed will start from its original position.
    ///   - initial_alpha: Initial alpha of the view prior to the animation.
    ///   - final_alpha: View's alpha after the animation.
    ///   - delay: Time Delay before the animation.
    ///   - duration: TimeInterval the animation takes to complete.
    ///   - damping_ratio: The damping ratio for the spring animation.
    ///   - velocity: The initial spring velocity.
    ///   - completion: CompletionBlock after the animation finishes.
     func animate(_ animations: [Animation],
                           reversed: Bool = false,
                           initia_alpha: CGFloat = 0.0,
                           final_alpha: CGFloat = 1.0,
                           delay: Double = 0,
                           duration: TimeInterval = AnimationDefaultConfigues.duration,
                           using_spring_with_damping damping_ratio: CGFloat = AnimationDefaultConfigues.spring_dampin_ratio,
                           initial_spring_velocity velocity: CGFloat = AnimationDefaultConfigues.inital_spring_velocity,
                           options: UIView.AnimationOptions = [],
                           completion: (() -> Void)? = nil){
        
        let transform_from = transform
        var transform_to   = transform
        
        animations.forEach { transform_to = transform_to.concatenating($0.intial_transform) }
        
        if !reversed {
            
            transform = transform_to
            
        }
        
        alpha = initia_alpha
        
        UIView.animate(withDuration: duration,
                       delay: delay,
                       usingSpringWithDamping: damping_ratio,
                       initialSpringVelocity: velocity,
                       options: options,
                       animations: {
                        [weak self] in
                        self?.transform = reversed ? transform_to : transform_from
                        self?.alpha = final_alpha
        }) { (_) in
            completion?()
        }
    }
    
    /// - Parameters:
    ///   - animations: Array of Animations to perform on the animation block.
    ///   - reversed: Initial state of the animation. Reversed will start from its original position.
    ///   - initialAlpha: Initial alpha of the view prior to the animation.
    ///   - finalAlpha: View's alpha after the animation.
    ///   - delay: Time Delay before the animation.
    ///   - animationInterval: Interval between the animations of each view.
    ///   - duration: TimeInterval the animation takes to complete.
    ///   - dampingRatio: The damping ratio for the spring animation.
    ///   - velocity: The initial spring velocity.
    ///   - completion: CompletionBlock after the animation finishes.
     func animate(on views: [UIView],
                 _ animations: [Animation],
                 reversed: Bool = false,
                 initia_alpha: CGFloat = 0.0,
                 final_alpha: CGFloat = 1.0,
                 delay: Double = 0,
                 animation_interval: TimeInterval = 0.05,
                 duration: TimeInterval = AnimationDefaultConfigues.duration,
                 using_spring_with_damping damping_ratio: CGFloat = AnimationDefaultConfigues.spring_dampin_ratio,
                 initial_spring_velocity velocity: CGFloat = AnimationDefaultConfigues.inital_spring_velocity,
                 options: UIView.AnimationOptions = [],
                 completion: (() -> Void)? = nil){
        
        guard views.count > 0 else{
            
            completion?()
            
            return
        }
        
        views.forEach { $0.alpha = initia_alpha }
        
        let dispatch_group = DispatchGroup()
        
        for _ in 1...views.count { dispatch_group.enter() }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay){
            
            for (index, view) in views.enumerated(){
                
                view.alpha = initia_alpha
                
                view.animate(animations,
                             reversed: reversed,
                             initia_alpha: initia_alpha,
                             final_alpha: final_alpha,
                             delay: Double(index) * animation_interval,
                             duration: duration,
                             using_spring_with_damping: damping_ratio,
                             initial_spring_velocity: velocity,
                             options: options){
                                dispatch_group.leave()
                }
            }
        }
        
        dispatch_group.notify(queue: .main){
            completion?()
        }
        
        
        
    }
    
    
    
    
    
}
