//
//  UIViewExtension.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

extension UIView {

    public func circulateSquarelyView(){
        
          if self.layer.bounds.width == self.layer.bounds.height {
            
            self.layer.cornerRadius  = self.layer.bounds.width / 2.0
              self.layer.masksToBounds = true
            
          }else{
            
              print("------------------------------------------------")
              print("debug description:")
              print("some of your given view arent square so circulation procces failed.views are listed bellow:")
              print("\(self)")
              print("The Width: \(self.frame.size.width)")
              print("The Height: \(self.frame.size.height)")
              print("------------------------------------------------")
            
          }
        

      }

    public func addDashedBorder(with_color:  UIColor){
        let border_color = with_color.cgColor
        let shape_layer  = CAShapeLayer()
        let frame_size   = self.frame.size
        let shape_rect   = CGRect(x: 0, y: 0, width: frame_size.width, height: frame_size.height)

        shape_layer.bounds          = shape_rect
        shape_layer.position        = CGPoint(x: frame_size.width/2, y: frame_size.height/2)
        shape_layer.fillColor       = UIColor.clear.cgColor
        shape_layer.strokeColor     = border_color
        shape_layer.lineWidth       = 1
        shape_layer.lineJoin        = CAShapeLayerLineJoin.bevel
        shape_layer.lineDashPattern = [4,3]
        shape_layer.path            = UIBezierPath(roundedRect: shape_rect, cornerRadius: 5).cgPath

        self.layer.addSublayer(shape_layer)
        
    }
    
    public func addDashedLine(with_color: UIColor){
        let  path = UIBezierPath()

        let  p0 = CGPoint(x: self.bounds.minX, y: self.bounds.midY)
        path.move(to: p0)

        let  p1 = CGPoint(x: self.bounds.maxX, y: self.bounds.midY)
        path.addLine(to: p1)

        let  dashes: [ CGFloat ] = [ 16.0, 32.0 ]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)

        path.lineWidth = 8.0
        path.lineCapStyle = .butt
        UIColor.magenta.set()
        path.stroke()
    }
    
    public func drawDottedLine(start p0: CGPoint, end p1: CGPoint) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = #colorLiteral(red: 0.8078431373, green: 0.662745098, blue: 0.5058823529, alpha: 1).cgColor
        shapeLayer.lineWidth = 0.75
        shapeLayer.lineDashPattern = [2, 2] // the first is the length of dash, second is length of the gap.

        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }

    //FortuneWheel
    private static let kRotationAnimationKey = "rotationanimationkey"
    
    func rotate(duration: Double = 1 , repeatTime : Float) {
        if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            let random = Float.random(in: 0...10)
            print(random)
            
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float.pi
            rotationAnimation.duration = 3
            rotationAnimation.repeatCount = 1
            print(rotationAnimation.toValue ?? "default value")
            layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
            
        }
    }
    
    func stopRotating() {
        if layer.animation(forKey: UIView.kRotationAnimationKey) != nil {
            layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
        }
    }
    
    
}
