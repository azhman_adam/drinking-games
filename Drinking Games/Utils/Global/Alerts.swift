//
//  Alerts.swift
//  Drinking Games
//
//  Created by cain on 3/28/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

struct Alert {
    
    private static func showBasicAlert(on vc: UIViewController, with title: String, message: String ){
        
        let blurEffect         = UIBlurEffect(style: .dark)
        let blur_effect_view   = UIVisualEffectView(effect: blurEffect)
        blur_effect_view.frame = vc.view.bounds
        let alert              = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.setBackgroundColor(color: #colorLiteral(red: 0.1767598987, green: 0.1647417545, blue: 0.1604485512, alpha: 1))
        alert.setTitle(font: UIFont.boldSystemFont(ofSize: 20), color: .white)
        alert.setMessage(font: UIFont.systemFont(ofSize: 15), color: .white)
      
        
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            blur_effect_view.removeFromSuperview()
        }
       

  
        
        alert.addAction(action)
        DispatchQueue.main.async {
            vc.view.addSubview(blur_effect_view)
            vc.present(alert, animated: true)
        }
    }
    
    static func enterMorehPlayers(on vc:UIViewController){
        
        showBasicAlert(on: vc, with: "", message: "Add at least 2 players")
    }
    
    
    static func initialWarning(on vc: UIViewController){
        showBasicAlert(on: vc, with: "Warning", message: "Please drink responsibly. By Continuing, you agree that you are responsible for any consequnces that may result from the use of Drinking Game App")
    }
    
    static func playersNameShouldBeUnique(on vc: UIViewController){
        showBasicAlert(on: vc, with: "", message: "Please enter unique names")
    }
    
    
}
