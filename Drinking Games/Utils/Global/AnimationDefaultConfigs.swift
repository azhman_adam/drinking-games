//
//  AnimationDefaultConfigs.swift
//  Drinking Games
//
//  Created by cain on 3/31/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

struct AnimationDefaultConfigues {
    
    //MARK: - Vars
    /// Amount of movement in points which depends on given direction to animation type
    public static var offset: CGFloat              = 30.0
    
    public static var duration: Double             = 0.3
    
    ///Interval for nimation handling multiple views so to be animated one after another not all at the same time
    public static var interval: Double             = 0.075
    
    ///Maximum zoom to be applied in animations using random AnimationType.zoom.
    public static var max_zoom_scale: Double       = 2.0
    
    ///Maximum rotation (left or right) to be applied in animations using random AnimationType.rotate
    public static var max_rotation_angle: CGFloat  = CGFloat.pi/4
    
    ///The damping ratio for the spring animation as it approaches its quiescent state.
    public static var spring_dampin_ratio: CGFloat = 1
    
    ///The initial spring velocity. For smooth start to the animation, match this value to the view’s velocity as it was prior to attachment.
    public static var inital_spring_velocity: CGFloat = 0
    
}
