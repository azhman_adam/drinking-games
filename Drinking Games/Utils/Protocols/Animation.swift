//
//  Animation.swift
//  Drinking Games
//
//  Created by cain on 3/31/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

protocol Animation {
    
    /// Defines the starting point for the animations.
    var intial_transform: CGAffineTransform{get}
    
}
