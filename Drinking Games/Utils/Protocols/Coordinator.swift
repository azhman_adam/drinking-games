//
//  Coordinator.swift
//  Drinking Games
//
//  Created by cain on 3/21/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {
    var navigation_controller: UINavigationController{get set}
    var child_coordinators: [Coordinator]{get set}
    
    func start()
}
