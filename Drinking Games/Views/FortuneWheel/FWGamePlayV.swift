//
//  FWGamePlayV.swift
//  Drinking Games
//
//  Created by cain on 5/3/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MAKR: - Imports
import UIKit

class FWGamePlayV: UIView {

    //MARK: - Vars
    private weak var _vc: FWGamePlayVC?
    private let _title: UILabel                = {
            let lbl = UILabel()
            
            lbl.text          = "Fortune Wheel"
            lbl.font          = rubik.black.with(Size: 32)
            lbl.textColor     = #colorLiteral(red: 0.3764705882, green: 0.2549019608, blue: 0.1803921569, alpha: 1)
            lbl.textAlignment = .center
            
            
            return lbl
        }()
    private let _back_button: UIButton         = {
        let btn = UIButton()
            
        btn.setImage(#imageLiteral(resourceName: "back_icon_dark"), for: .normal)
            
        return btn
    }()
    private let _game_container: UIView        = {
        let view = UIView()
        
        view.layer.cornerRadius = 60
        view.layer.masksToBounds = true
        
        return view
    }()
    private let _play_button: UIButton         = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "reload"), for: .normal)
        
        btn.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
        
        btn.setShadow(with_color: .black, opacity: 0.24, radius: 16, offset_width: 0, offset_height: 6)
        
        btn.layer.masksToBounds = false
        btn.clipsToBounds = false
        return btn
    }()
    private let _play_container: UIView        = {
        let view = UIView()
        
        view .backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
        
        return view
    }()
    private let _play_button_container: UIView = {
        let view = UIView()
        
        view.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.8509803922, blue: 0.7725490196, alpha: 1)
        
        view.layer.masksToBounds = false
        view.clipsToBounds = false
        return view
    }()
    
    private let _fortune_frame: UIImageView   = {
        let img_v = UIImageView()
        
        img_v.image = #imageLiteral(resourceName: "FortuneWheelFrame")
        img_v.contentMode = .scaleAspectFit
        
        return img_v
    }()
    private let _fortune_stand: UIImageView   = {
        let img_v = UIImageView()
        
        img_v.image = #imageLiteral(resourceName: "FortuneWheelStand")
        
        img_v.contentMode = .scaleAspectFit
        
        return img_v
    }()
    private let _fortune_play: UIButton       = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "FortuneWheelSpin"), for: .normal)
        
        return btn
    }()
    private let _fortune_pointer: UIImageView = {
        let img_v = UIImageView()
        
        img_v.image = #imageLiteral(resourceName: "FortuneWheelPointer")
        
        return img_v
    }()
    
    var slices = [ FortuneWheelSlice(title: "Drink 2 Sips before start"),
                    FortuneWheelSlice(title: "Kiss a girl"),
                    FortuneWheelSlice(title: "Kiss a boy"),
                    FortuneWheelSlice(title: "Choose one to drink a sip"),
                    ]
    
    var fortune_wheel: TTFortuneWheel = {
        
        var slices = [ FortuneWheelSlice(title: "Drink 2 Sips before start"),
                        FortuneWheelSlice(title: "Kiss a girl"),
                        FortuneWheelSlice(title: "Kiss a boy"),
                        FortuneWheelSlice(title: "Choose one to drink a sip"),
                        ]

        
        let view = TTFortuneWheel(frame: .zero, slices: slices)
        
        view.equalSlices       = true
        view.frameStroke.width = 10
        view.frameStroke.color = .white
        
        
        return view
    }()
    
    
    //MARK: - Getter, Setter
    func setVC(_ vc: FWGamePlayVC){
        
        self._vc = vc
    }
    
    //MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        mainConfigurations()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    //MARK: - LayOuting
    private func setupViews(){
        setupTopQuarterViews()
        setupTopMiddleQuarterViews()
        setupBottomMiddleQuarterViews()
        setupBottomQuarterViews()
        
    }
    
    private func setupTopQuarterViews(){
        addSubview(_game_container)
        addSubview(_title)
        addSubview(_back_button)
        
        _game_container.anchor(top: topAnchor, bottom: bottomAnchor, right: rightAnchor, left: leftAnchor, top_constant: -25, bottom_constant: 160, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
        
        _title.anchorWithXCenter(top: topAnchor, bottom: nil, top_constant: 65, bottom_constant: 0, width_constant: 0, height_constant: 0, center_x_with: self, x_constant: 0)
        
        _back_button.anchorWithYCenter(right: nil, left: leftAnchor, right_constant: 0, left_constant: 32, width_constant: 25, height_constant: 25, center_y_with: _title, y_constant: 0)
    }
    
    private func setupTopMiddleQuarterViews(){
        
        
        addSubview(_fortune_frame)
        addSubview(fortune_wheel)
        addSubview(_fortune_play)
        addSubview(_fortune_pointer)
        
        
        _fortune_frame.anchorCenter(center_with: self, y_constant: -60,width: 338, height: 361)
        
        fortune_wheel.anchorCenter(center_with: _fortune_frame, width: 275, height: 288)
        
        
        _fortune_play.anchorCenter(center_with: _fortune_frame, width: 81, height: 81)
        
        _fortune_pointer.anchorWithYCenter(right: _fortune_frame.leftAnchor, left: nil, right_constant: -68, left_constant: 0, width_constant: 81, height_constant: 81, center_y_with: _fortune_frame, y_constant: 0)
        
    }
    
    private func setupBottomMiddleQuarterViews(){
        addSubview(_fortune_stand)
        
        _fortune_stand.anchorWithXCenter(top: _fortune_frame.bottomAnchor, bottom: nil, top_constant: -60, bottom_constant: 0, width_constant: 313, height_constant: 163, center_x_with: self, x_constant: 0)
        
       
        

    }
    
    private func setupBottomQuarterViews(){
        
        addSubview(_play_container)
        addSubview(_play_button_container)
        addSubview(_play_button)
        
        _play_container.anchorWithXCenter(top: nil, bottom: _game_container.bottomAnchor, top_constant: 0, bottom_constant: -105, width_constant: 210, height_constant: 210, center_x_with: _game_container, x_constant: 0)
        
        _play_button_container.anchorCenter(center_with: _play_container, width: 165, height: 165)
        
        _play_button.anchorCenter(center_with: _play_button_container, width: 120, height: 120)
                
    }
    
    
    //MARK: - Configuration
    private func mainConfigurations(){
        
        backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
        
        addButtonsToTarget()
        
        self.bringSubviewToFront(_fortune_frame)
        self.bringSubviewToFront(_fortune_pointer)
    }
    
    private func addButtonsToTarget(){
        
        _play_button.addTarget(_vc.self, action: #selector(_vc?.playTheGame(_:)), for: .touchUpInside)
        _fortune_play.addTarget(_vc.self, action: #selector(_vc?.playTheGame(_:)), for: .touchUpInside)
        
    }
    
    func postConfigurations(){

        
        _game_container.setLinearGradient(with: [#colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1).cgColor, #colorLiteral(red: 0.9254901961, green: 0.8509803922, blue: 0.7725490196, alpha: 1).cgColor], from: CGPoint(x: 0.5, y: 0.0), to: CGPoint(x: 0.5, y: 1.0))
        
        _play_container.circulateSquarelyView()
        _play_button.circulateSquarelyView()
        _play_button_container.circulateSquarelyView()
        

        
    }
}
