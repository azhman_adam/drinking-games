//
//  InitialV.swift
//  Drinking Games
//
//  Created by cain on 3/21/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imprts
import UIKit

class InitialV: UIView {
    
    //MARK: - Vars
    weak var vc: InitialVC?
    
    private let lang_selection_btn: UIButton = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "flag_us"), for: .normal)
        btn.layer.borderWidth = 1.5
        btn.layer.borderColor = #colorLiteral(red: 0.9882352941, green: 0.6745098039, blue: 0.3058823529, alpha: 1)
        
        return btn
    }()
    private let purchase_btn: UIButton       = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "purch"), for: .normal)
        btn.pulsate()
        btn.flash()
        
        return btn
    }()
    private let app_name: UILabel            = {
        let lbl = UILabel()
        
        lbl.text          = "Lets Drink"
        lbl.textColor     = #colorLiteral(red: 0.9254901961, green: 0.8509803922, blue: 0.7725490196, alpha: 1)
        lbl.font          = rubik.black.with(Size: 63)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        
        lbl.setShadow(with_color: #colorLiteral(red: 0.9254901961, green: 0.8509803922, blue: 0.7725490196, alpha: 1), opacity: 0.3, radius: 6, offset_width: 0, offset_height: 3)
        
        return lbl
    }()
    
    private let nevermind_container: RadialGradientView = {
        let view = RadialGradientView()
        
        view.inside_color  = #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1)
        view.outside_color = #colorLiteral(red: 0.9333333333, green: 0.6156862745, blue: 0.06274509804, alpha: 1)
        
        view.setShadow(with_color: #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1), opacity: 0.2, radius: 16, offset_width: 0, offset_height: 4)
        
        view.layer.cornerRadius  = 26
        view.layer.masksToBounds = true
        
        return view
    }()
    private let nevermind_image: UIImageView            = {
        let img_v = UIImageView()
        
        img_v.image = #imageLiteral(resourceName: "bear_img")
        
        
        return img_v
    }()
    private let nevermind_label: UILabel                = {
        let lbl = UILabel()
        
        lbl.text          = "Never Have I Ever"
        lbl.textAlignment = .left
        lbl.font          = rubik.black.with(Size: 24)
        lbl.numberOfLines = 2
        
        lbl.setShadow(with_color: .black, opacity: 0.16, radius: 6, offset_width: 0, offset_height: 3)
        
        return lbl
    }()
    private let nevermind_button: UIButton              = {
        let btn = UIButton()
        
        btn.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        
        btn.setTitle("Play Now", for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0.9411764706, green: 0.6196078431, blue: 0, alpha: 1), for: .normal)
        
        btn.titleLabel?.font = rubik.bold.with(Size: 13)
        
        btn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        
        btn.imageView?.layer.masksToBounds = true
        btn.layer.cornerRadius             = 18
        btn.layer.masksToBounds            = true
        
        return btn
    }()
    
    private let wheel_container: RadialGradientView = {
        let view = RadialGradientView()
        
        view.inside_color  = #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1)
        view.outside_color = #colorLiteral(red: 0.9333333333, green: 0.6156862745, blue: 0.06274509804, alpha: 1)
        
        view.setShadow(with_color: #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1), opacity: 0.2, radius: 16, offset_width: 0, offset_height: 4)
        
        view.layer.cornerRadius  = 12
        view.layer.masksToBounds = true
        
        return view
    }()
    private let wheel_image: UIImageView            = {
        let img = UIImageView()
        
        img.image = #imageLiteral(resourceName: "fortune_wheel")
        
        return img
    }()
    private let wheel_label: UILabel                = {
        let lbl = UILabel()
        
        lbl.text          = "Fortune Wheel"
        lbl.numberOfLines = 2
        lbl.font          = rubik.black.with(Size: 18)
        lbl.textAlignment = .center
        lbl.textColor     = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        
        return lbl
    }()
    private let wheel_button: UIButton              = {
        let btn = UIButton()
        
        btn.titleLabel?.text = ""
        
        return btn
    }()
    
    private let bottle_container: RadialGradientView = {
        let view = RadialGradientView()
        
        view.inside_color  = #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1)
        view.outside_color = #colorLiteral(red: 0.9333333333, green: 0.6156862745, blue: 0.06274509804, alpha: 1)
        
        view.setShadow(with_color: #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1), opacity: 0.2, radius: 16, offset_width: 0, offset_height: 4)
        
        view.layer.cornerRadius  = 12
        view.layer.masksToBounds = true
        
        return view
    }()
    private let bottle_image: UIImageView            = {
        let img = UIImageView()
        
        img.image = #imageLiteral(resourceName: "lucky_bottle")
        
        return img
    }()
    private let bottle_label: UILabel                = {
        let lbl = UILabel()
        
        lbl.text          = "Lucky Bottle"
        lbl.numberOfLines = 2
        lbl.font          = rubik.black.with(Size: 18)
        lbl.textAlignment = .center
        lbl.textColor     = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        
        return lbl
    }()
    private let bottle_button: UIButton              = {
        let btn = UIButton()
        
        btn.titleLabel?.text = ""
        
        return btn
    }()
    
    private let taproulette_container: RadialGradientView = {
        let view = RadialGradientView()
        
        view.inside_color  = #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1)
        view.outside_color = #colorLiteral(red: 0.9333333333, green: 0.6156862745, blue: 0.06274509804, alpha: 1)
        
        view.setShadow(with_color: #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1), opacity: 0.2, radius: 16, offset_width: 0, offset_height: 4)
        
        view.layer.cornerRadius  = 12
        view.layer.masksToBounds = true
        
        return view
    }()
    private let taproulette_image: UIImageView            = {
        let img = UIImageView()
        
        img.image = #imageLiteral(resourceName: "tap_roulette")
        
        return img
    }()
    private let taproulette_label: UILabel                = {
        let lbl = UILabel()
        
        lbl.text          = "Tap Roulette"
        lbl.numberOfLines = 2
        lbl.font          = rubik.black.with(Size: 18)
        lbl.textAlignment = .center
        lbl.textColor     = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        
        return lbl
    }()
    private let taproulette_button: UIButton              = {
        let btn = UIButton()
        
        btn.titleLabel?.text = ""
        
        return btn
    }()
    
    //MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        maindConfigurations()
        setupViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - LayOuting
    private func setupViews(){
        
        setupTopQuarteViews()
        setupTopMiddlQuarterViews()
        setupBottomMiddleQuarterViews()
        setupBottomQuarterViews()
        
    }
    
    private func setupTopQuarteViews(){
        
        addSubview(lang_selection_btn)
        addSubview(purchase_btn)
        addSubview(app_name)
        
        lang_selection_btn.anchor(top: self.safeAreaLayoutGuide.topAnchor, bottom: nil, right: nil, left: self.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 40, width_constant: 35, height_constant: 35)
        
        purchase_btn.anchorWithYCenter(right: self.rightAnchor, left: nil, right_constant: 40, left_constant: 0, width_constant: 35, height_constant: 25, center_y_with: lang_selection_btn, y_constant: 0)
        
        app_name.anchorWithXCenter(top: lang_selection_btn.bottomAnchor, bottom: nil, top_constant: 0, bottom_constant: 0, width_constant: 230, height_constant: 0, center_x_with: self, x_constant: 0)
        
        
    }
    
    private func setupTopMiddlQuarterViews(){
        
        addSubview(nevermind_container)
        nevermind_container.addSubview(nevermind_label)
        addSubview(nevermind_image)
        nevermind_container.addSubview(nevermind_button)
        
        nevermind_container.anchorWithXCenter(top: app_name.bottomAnchor, bottom: nil, top_constant: 55, bottom_constant: 0, width_constant: 315, height_constant: 175, center_x_with: self, x_constant: 0)
        
        nevermind_label.anchor(top: nevermind_container.topAnchor, bottom: nil, right: nil, left: nevermind_container.leftAnchor, top_constant: 28, bottom_constant: 0, right_constant: 0, left_constant: 24, width_constant: 140, height_constant: 0)
        
        nevermind_image.anchor(top: nevermind_container.topAnchor, bottom: nil, right: nevermind_container.rightAnchor, left: nil, top_constant: -46, bottom_constant: 0, right_constant: -50, left_constant: 0, width_constant: 185, height_constant: 175)
        
        nevermind_button.anchorWithXCenter(top: nil, bottom: nevermind_container.bottomAnchor, top_constant: 0, bottom_constant: 20, width_constant: 115, height_constant: 35, center_x_with: nevermind_label, x_constant: 0)
        
        
        
    }
    
    private func setupBottomMiddleQuarterViews(){
        
        addSubview(wheel_container)
        wheel_container.addSubview(wheel_label)
        
        
        
        addSubview(bottle_container)
        bottle_container.addSubview(bottle_label)
    
        
        addSubview(taproulette_container)
        taproulette_container.addSubview(taproulette_label)
        
        
        let arrange_subviews = [wheel_container, bottle_container, taproulette_container]
        
        let stack_view: UIStackView = {
            let stack = UIStackView(arrangedSubviews: arrange_subviews)
            
            stack.axis         = .horizontal
            stack.alignment    = .fill
            stack.spacing      = 15
            stack.distribution = .fillEqually
            
            stack.translatesAutoresizingMaskIntoConstraints = false

            return stack
        }()
        
        addSubview(stack_view)
        addSubview(wheel_image)
        addSubview(bottle_image)
        addSubview(taproulette_image)
        addSubview(wheel_button)
        addSubview(bottle_button)
        addSubview(taproulette_button)

        
        stack_view.anchor(top: nevermind_container.bottomAnchor, bottom: nil, right: nevermind_container.rightAnchor , left: nevermind_container.leftAnchor, top_constant: 80, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 144)
        
        wheel_label.anchor(top: nil, bottom: wheel_container.bottomAnchor, right: wheel_container.rightAnchor, left: wheel_container.leftAnchor, top_constant: 0, bottom_constant: 17, right_constant: 5, left_constant: 5, width_constant: 0, height_constant: 0)
        
        wheel_image.anchor(top: nil, bottom: wheel_label.topAnchor, right: nil, left: wheel_container.leftAnchor, top_constant: 0, bottom_constant: 12, right_constant: 0, left_constant: 12, width_constant: 97, height_constant: 97)
        
        wheel_button.anchor(top: wheel_image.topAnchor, bottom: wheel_container.bottomAnchor, right: wheel_image.rightAnchor, left: wheel_container.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
        
        bottle_label.anchor(top: nil, bottom: bottle_container.bottomAnchor, right: bottle_container.rightAnchor, left: bottle_container.leftAnchor, top_constant: 0, bottom_constant: 17, right_constant: 5, left_constant: 5, width_constant: 0, height_constant: 0)
        
        bottle_image.anchorWithXCenter(top: nil, bottom: bottle_label.topAnchor, top_constant: 0, bottom_constant: 12, width_constant: 62, height_constant: 110, center_x_with: bottle_container, x_constant: 0)
        
        bottle_button.anchor(top: bottle_image.topAnchor, bottom: bottle_container.bottomAnchor, right: bottle_container.rightAnchor, left: bottle_container.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
        
        taproulette_label.anchor(top: nil, bottom: taproulette_container.bottomAnchor, right: taproulette_container.rightAnchor, left: taproulette_container.leftAnchor, top_constant: 0, bottom_constant: 17, right_constant: 5, left_constant: 5, width_constant: 0, height_constant: 0)
        
        taproulette_image.anchor(top: nil, bottom: taproulette_label.topAnchor, right: nil, left: taproulette_container.leftAnchor, top_constant: 0, bottom_constant: 17, right_constant: 0, left_constant: -15, width_constant: 85, height_constant: 100)
        
        taproulette_button.anchor(top: taproulette_image.topAnchor, bottom: taproulette_container.bottomAnchor, right: taproulette_container.rightAnchor, left: taproulette_image.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
    }
    
    private func setupBottomQuarterViews(){}
    
    
    //MARK: - Configurations
    private func maindConfigurations(){
        
        self.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        addButtonToTarget()
    }
    
    func postConfigurations(){
        
        lang_selection_btn.circulateSquarelyView()
        nevermindButtonUIEdgeInset()
        
    }
    
    private func nevermindButtonUIEdgeInset(){
        
        let image_width = nevermind_button.imageView?.frame.width
        let btn_width   = nevermind_button.frame.width
        
        nevermind_button.imageEdgeInsets = UIEdgeInsets(top: -1, left: ((btn_width) - image_width! * 2), bottom: 0, right: -((btn_width/2) - image_width!))


        nevermind_button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -image_width!, bottom: 0, right: image_width!)
        
        
    }
    
    private func addButtonToTarget(){
        
        lang_selection_btn.addTarget(vc.self, action: #selector(vc?.langSelectionMode(_:)), for: .touchUpInside)
        purchase_btn.addTarget(vc.self, action: #selector(vc?.goToPurchase(_:)), for: .touchUpInside)
        nevermind_button.addTarget(vc.self, action: #selector(vc?.goToNevermind(_:)), for: .touchUpInside)
        wheel_button.addTarget(vc.self, action: #selector(vc?.goToFortunewheel(_:)), for: .touchUpInside)
        bottle_button.addTarget(vc.self, action: #selector(vc?.goToLuckybottle(_:)), for: .touchUpInside)
        taproulette_button.addTarget(vc.self, action: #selector(vc?.goToTaproulette(_:)), for: .touchUpInside)
        
        
    }
    
    
  
}
