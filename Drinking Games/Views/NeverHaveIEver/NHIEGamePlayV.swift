//
//  NHIEGamePlayV.swift
//  Drinking Games
//
//  Created by cain on 4/4/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class NHIEGamePlayV: UIView {

    //MARK: - Vars
    private weak var _vc: NHIEGamePlayVC?
    
    private var _round_header: UILabel   = {
        let lbl = UILabel()
        
        lbl.font = rubik.black.with(Size: 118)
        lbl.text = "Round 1"
        
        lbl.setShadow(with_color: #colorLiteral(red: 0.6588235294, green: 0.5176470588, blue: 0, alpha: 1), opacity: 1, radius: 6, offset_width: -7, offset_height: 7)
        
        lbl.numberOfLines = 0
        lbl.textColor     = .white
        
        return lbl
    }()
    private var _end_button: UIButton    = {
        
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        
        return btn
    }()
    private var _round_title: UILabel    = {
        
        let lbl = UILabel()
        
        lbl.text          = "Drink 1 Sip"
        lbl.textColor     = .white
        lbl.numberOfLines = 1
        lbl.font          = rubik.bold.with(Size: 32)
        
        lbl.setShadow(with_color: .black, opacity: 0.16, radius: 6, offset_width: 0, offset_height: 3)
        lbl.isUserInteractionEnabled = true
        
        return lbl
        
    }()
    private var _question_label: UILabel = {
        let lbl = UILabel()
        
        lbl.font          = rubik.medium.with(Size: 32)
        lbl.textAlignment = .center
        lbl.numberOfLines = 3
        lbl.textColor     = .white
        
        return lbl
    }()
    private var center_header: CGRect!

    
    
    //MARK: - Getter,Setter
    func setViewController(_ vc: NHIEGamePlayVC){
            _vc = vc
    }
    func setQuestion(text: String, first: Bool){
        if first{
            _question_label.text = text
        }else{
            _question_label.text = text
            print(text)
            questionShowAnimation()
        }
    }
    
    
    //MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
         self.isUserInteractionEnabled = true
        setupViews()
        mainConfigurations()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LayOuting
    private func setupViews(){
        
        addSubview(_round_header)
        addSubview(_end_button)
        addSubview(_round_title)
        addSubview(_question_label)
        
        setcont()
        
        _end_button.anchor(top: self.topAnchor, bottom: nil, right: nil, left: self.leftAnchor, top_constant: 30, bottom_constant: 0, right_constant: 0, left_constant: 30, width_constant: 17, height_constant: 17)

        _round_title.anchorWithXCenter(top: self.topAnchor, bottom: nil, top_constant: 30, bottom_constant: 0, width_constant: 0, height_constant: 0, center_x_with: self, x_constant: 0)

        _question_label.anchorCenter(center_with: self, x_constant: 0, y_constant: 0, width: CGFloat(UIScreen.main.bounds.height - 80), height: 150)
        
        center_header = _round_header.frame
      
    }
    
    //MARK: - Configurations
    private func mainConfigurations(){
        
        backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.7058823529, blue: 0, alpha: 1)
       
    }
    
    func postConfigurations(){
        
        addTargetToButtons()
        addEventToViews()
      
    }
    
    func startTheRound(number: Round, show_state: Bool){
        switch number {
            case .one:
                if !show_state{
                    priorToRoundConfigurations()
                }else{
                    
                    _round_header.text = "Round 1"
                    
                    initialConfigurations()
            }
            case .two:
                    if !show_state{
                        _round_header.text = "Round 2"
                        backgroundColor = #colorLiteral(red: 0, green: 0.2823529412, blue: 0.1725490196, alpha: 1)
                        _round_header.removeShadow()
                        _round_header.setShadow(with_color: #colorLiteral(red: 0, green: 0.1019607843, blue: 0.05882352941, alpha: 1), opacity: 1, radius: 6, offset_width: -7, offset_height: 7)
                        
                        _round_title.text = "GIVE OUT 2 SIPS"
                        
                        _round_title.removeShadow()
                        _round_title.setShadow(with_color: #colorLiteral(red: 0, green: 0.1019607843, blue: 0.05882352941, alpha: 1), opacity: 1, radius: 0, offset_width: 3, offset_height: 6)
                        
                        priorToRoundConfigurations()
                    }else{

                        initialConfigurations()
                        
                }

            case .three:
                    if !show_state{
                        _round_header.text = "Round 3"
                        backgroundColor = #colorLiteral(red: 0.2509803922, green: 0.003921568627, blue: 0.003921568627, alpha: 1)
                        _round_header.removeShadow()
                        _round_header.setShadow(with_color: #colorLiteral(red: 0.0862745098, green: 0, blue: 0, alpha: 1), opacity: 1, radius: 6, offset_width: -7, offset_height: 7)
                        
                        //set title text
                        
                        _round_title.removeShadow()
                        _round_title.setShadow(with_color: #colorLiteral(red: 0.1176470588, green: 0, blue: 0, alpha: 1), opacity: 1, radius: 6, offset_width: 0, offset_height: 3)
                        
                        
                        priorToRoundConfigurations()
                    }else{
                        
                       
                        
                        initialConfigurations()
                }
        }
    }
    
    private func initialConfigurations(){
        
        roundIntroAnimation(round_header: _round_header) {
            self.afterRoundConfigurations()
        }
    }
    
    private func addTargetToButtons(){
        
        _end_button.addTarget(_vc.self, action: #selector(_vc?.endButtonTouchUpInside), for: .touchUpInside)
        
    }
    
    private func addEventToViews(){
        
        let tap = UITapGestureRecognizer(target: _vc.self, action: #selector(_vc?.nextQuestionIsRequested(_:)))
        self.addGestureRecognizer(tap)
       
    }
    
    private func priorToRoundConfigurations(){
        
        _round_header.isHidden    = false
        _round_title.isHidden     = true
        _end_button.isHidden      = true
        _end_button.isEnabled     = false
        _question_label.isHidden  = true
        
        
    }
    
    private func afterRoundConfigurations(){
        
        _round_header.isHidden    = true
        _round_title.isHidden     = false
        _end_button.isHidden      = false
        _end_button.isEnabled     = true
        _question_label.isHidden  = false
        _question_label.isEnabled = true
        
        questionShowAnimation()
        
    }
    
    
    //MARK: - Animations
    private func roundIntroAnimation(round_header: UILabel, completionHandler: @escaping () -> Void){
        
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            round_header.center.x = self.center.x
            print("first")
        }) { (_) in
            UIView.animate(withDuration: 0.25, delay: 0.25, usingSpringWithDamping: 3, initialSpringVelocity: 0.5, options: .curveLinear, animations: {
                round_header.center.x -= 25
                print("Second")
            }) { (_) in
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
                    round_header.center.x += UIScreen.main.bounds.width
                }) { (_) in
                    print("this")
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.75) {
            print("that")
            self.setcont()
            completionHandler()
        }

    }
    
   

    private func questionShowAnimation(){
        
    
        let center = _question_label.center.x
        
        UIView.animate(withDuration: 0.09, delay: 0, options: .curveLinear, animations: {
            self._question_label.center.x -= 20
        }) { (_) in
            UIView.animate(withDuration: 0.09, delay: 0, options: .curveLinear, animations: {
                self._question_label.center.x += 40
            }) { (_) in
                UIView.animate(withDuration: 0.09, delay: 0, options: .curveLinear, animations: {
                    self._question_label.center.x = center
                }) { (_) in
                    
                }
            }
        }
        
        
    }
    

    private func setcont(){
        _round_header.removeConstraints(_round_header.constraints)
        _round_header.anchorWithYCenter(right: leftAnchor, left: nil, right_constant: 0, left_constant: 0, width_constant: 530, height_constant: 140, center_y_with: self, y_constant: 0)
        layoutIfNeeded()
    }
 
}


