//
//  NHIEGameSelectionV.swift
//  Drinking Games
//
//  Created by cain on 3/28/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class NHIEGameSelectionV: UIView {

    //MARK: - Vars
    weak var vc: NHIEGameSelectionVC?
    private let back_button: UIButton        = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "back_icon_light"), for: .normal)
        
        return btn
    }()
    private let nope_button: UIButton        = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "nope_icon_dark"), for: .normal)
        
        btn.backgroundColor     = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
        btn.layer.cornerRadius  = 20
        btn.layer.masksToBounds = true
        
        return btn
    }()
    private let custom_button: UIButton      = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "custom_icon_light"), for: .normal)
        
        btn.layer.cornerRadius  = 20
        btn.layer.masksToBounds = true
        btn.backgroundColor     = .clear
        
        return btn
    }()
    private let silly_button: UIButton       = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "silly_icon_light"), for: .normal)
        
        btn.layer.cornerRadius  = 20
        btn.layer.masksToBounds = true
        btn.backgroundColor     = .clear
        
        return btn
    }()
    private let hot_button: UIButton         = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "hot_icon_light"), for: .normal)
        
        btn.layer.cornerRadius  = 20
        btn.layer.masksToBounds = true
        btn.backgroundColor     = .clear
        
        return btn
    }()
    private let springbreak_button: UIButton = {
        let btn = UIButton()
        
        btn.setImage(#imageLiteral(resourceName: "springbreak_icon_light"), for: .normal)
        
        btn.layer.cornerRadius  = 20
        btn.layer.masksToBounds = true
        btn.backgroundColor     = .clear
        
        return btn
    }()
    private let separator: UIView            = {
        let v = UIView()
        
        v.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 0.1)
            
        
        return v
    }()
    private let button_stack: UIStackView    = {
        let stack = UIStackView()
        
        stack.axis         = .vertical
        stack.distribution = .equalSpacing
        stack.alignment    = .center
        
        
        return stack
    }()
    
    private let game_mode_container: UIView  = {
        let v = UIView()
        
        v.backgroundColor     = #colorLiteral(red: 0.8078431373, green: 0.662745098, blue: 0.5058823529, alpha: 0.08899097711)
        v.layer.cornerRadius  = 15
        v.layer.masksToBounds = true
        
        return v
    }()
    private let game_mode_title: UILabel     = {
        let lbl = UILabel()
        
        lbl.font          = rubik.medium.with(Size: 24)
        lbl.textColor     = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
        lbl.numberOfLines = 1
        lbl.text          = nope_title
        lbl.textAlignment = .center
        
        return lbl
    }()
    private let game_mode_destail: UILabel   = {
        let lbl = UILabel()
        
        lbl.font          = rubik.regular.with(Size: 18)
        lbl.textColor     = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 0.6)
        lbl.textAlignment = .center
        lbl.text          = nope_deatil
        lbl.numberOfLines = 0
        
        return lbl
    }()
    private let game_mode_img: UIImageView   = {
        let img_v = UIImageView()
        
        img_v.image = UIImage(named: nope_image)
        img_v.contentMode = .scaleAspectFit
        
        return img_v
    }()
    private let game_mode_play_btn: UIButton = {
        let btn = UIButton()
        
        btn.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.5529411765, blue: 0.1215686275, alpha: 1)
        
        btn.setTitle("GO!", for: .normal)
        
        btn.titleLabel?.font      = rubik.bold.with(Size: 24)
        btn.titleLabel?.textColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
        btn.layer.cornerRadius    = 30
        btn.layer.masksToBounds   = true
        
        btn.setShadow(with_color: #colorLiteral(red: 0.9882352941, green: 0.6745098039, blue: 0.3058823529, alpha: 1), opacity: 0.6, radius: 20, offset_width: 0, offset_height: 18)
        
        return btn
    }()
    
 
    
    //MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        maninConfiguration()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - LayOuting
    private func setupViews(){
        setupLeftSideViews()
        setupRightSideViews()
    }
    
    private func setupLeftSideViews(){
        addSubview(nope_button)
        addSubview(custom_button)
        addSubview(silly_button)
        addSubview(hot_button)
        addSubview(springbreak_button)
        addSubview(separator)
        addSubview(button_stack)
        addSubview(back_button)
        
       let width  = CGFloat(40)
       let height = CGFloat(40)

        
        nope_button.anchorWidthAndHeight(width: width, height: height)
        custom_button.anchorWidthAndHeight(width: width, height: height)
        silly_button.anchorWidthAndHeight(width: width, height: height)
        hot_button.anchorWidthAndHeight(width: width, height: height)
        springbreak_button.anchorWidthAndHeight(width: width, height: height)
        
        button_stack.addArrangedSubview(nope_button)
        button_stack.addArrangedSubview(custom_button)
        button_stack.addArrangedSubview(silly_button)
        button_stack.addArrangedSubview(hot_button)
        button_stack.addArrangedSubview(springbreak_button)
        
        button_stack.anchorWithYCenter(right: nil, left: self.leftAnchor, right_constant: 0, left_constant: 16, width_constant: 40, height_constant: 550, center_y_with: self, y_constant: 0)
        
        separator.anchor(top: button_stack.topAnchor, bottom: button_stack.bottomAnchor, right: nil, left: button_stack.rightAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 15, width_constant: 2, height_constant: 0)
        
        back_button.anchorWithXCenter(top: self.safeAreaLayoutGuide.topAnchor, bottom: nil, top_constant: 15, bottom_constant: 0, width_constant: 25, height_constant: 25, center_x_with: button_stack, x_constant: 0)
        
    }
    
    private func setupRightSideViews(){
        addSubview(game_mode_container)
        game_mode_container.addSubview(game_mode_title)
        game_mode_container.addSubview(game_mode_destail)
        game_mode_container.addSubview(game_mode_img)
        game_mode_container.addSubview(game_mode_play_btn)
        
        game_mode_container.anchor(top: button_stack.topAnchor, bottom: button_stack.bottomAnchor, right: self.rightAnchor, left: separator.rightAnchor, top_constant: 0, bottom_constant: 0, right_constant: 25, left_constant: 25, width_constant: 0, height_constant: 0)
        
        game_mode_title.anchorWithYCenter(right: game_mode_container.rightAnchor, left: game_mode_container.leftAnchor, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0, center_y_with: nope_button, y_constant: 10)
        
        game_mode_destail.anchor(top: game_mode_title.bottomAnchor, bottom: nil, right: game_mode_container.rightAnchor, left: game_mode_container.leftAnchor, top_constant: 15, bottom_constant: 0, right_constant: 15, left_constant: 15, width_constant: 0, height_constant: 0)
        
        game_mode_img.anchorWithXCenter(top: game_mode_destail.bottomAnchor, bottom: nil, top_constant: 25, bottom_constant: 0, width_constant: 230, height_constant: 245, center_x_with: game_mode_container, x_constant: 0)
        
        game_mode_play_btn.anchorWithXCenter(top: nil, bottom: game_mode_container.bottomAnchor, top_constant: 0, bottom_constant: 20, width_constant: 180, height_constant: 60, center_x_with: game_mode_container, x_constant: 0)
        
        
        
        
    }
    
    
    //MARK: - Configurations
    private func maninConfiguration(){
        
        self.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        buttonsAddToTarget()
    }
    
    private func buttonsAddToTarget(){
        
        nope_button.addTarget(vc.self, action: #selector(vc?.nopeTouchUPInside(_:)), for: .touchUpInside)
        custom_button.addTarget(vc.self, action: #selector(vc?.customTouchUPInside(_:)), for: .touchUpInside)
        silly_button.addTarget(vc.self, action: #selector(vc?.sillyTouchUPInside(_:)), for: .touchUpInside)
        hot_button.addTarget(vc.self, action: #selector(vc?.hotTouchUPInside(_:)), for: .touchUpInside)
        springbreak_button.addTarget(vc.self, action: #selector(vc?.springbreakTouchUPInside(_:)), for: .touchUpInside)
        back_button.addTarget(vc.self, action: #selector(vc?.backTouchUpInside(_:)), for: .touchUpInside)
        game_mode_play_btn.addTarget(vc.self, action: #selector(vc?.goTouchUpInside(_:)), for: .touchUpInside)
    }
    
    public func postConfiguration(){
        
        
    }
    
    public func changeStyleToDark(selected_game_mode mode: GameMode){
        
     
        switch mode {
            case .nope:
                nope_button.backgroundColor = .clear
               
                nope_button.setImage(#imageLiteral(resourceName: "nope_icon_light"), for: .normal)
                nope_button.removeShadow()
            
            case .custom:
                custom_button.backgroundColor = .clear
            
                custom_button.setImage(#imageLiteral(resourceName: "custom_icon_light"), for: .normal)
                custom_button.removeShadow()
            
            case .silly:
                silly_button.backgroundColor = .clear
                
                silly_button.setImage(#imageLiteral(resourceName: "silly_icon_light"), for: .normal)
                silly_button.removeShadow()
                
            case .hot:
                hot_button.backgroundColor = .clear
            
                hot_button.setImage(#imageLiteral(resourceName: "hot_icon_light"), for: .normal)
                hot_button.removeShadow()
                
            case .springbreak:
                springbreak_button.backgroundColor = .clear
                
                springbreak_button.setImage(#imageLiteral(resourceName: "springbreak_icon_light"), for: .normal)
                springbreak_button.removeShadow()
    
        }
        
        
    }
    
    public func changeStyleToLight(selected_game_mode mode: GameMode){
        
        let pack = [game_mode_title,
                    game_mode_destail,
                    game_mode_img]
        
        switch mode {
            case .nope:
                nope_button.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
            
                nope_button.setImage(#imageLiteral(resourceName: "nope_icon_dark"), for: .normal)
                nope_button.setShadow(with_color: #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1), opacity: 0.15, radius: 20, offset_width: 0, offset_height: 6)
            
                game_mode_title.text   = nope_title
                game_mode_destail.text = nope_deatil
                game_mode_img.image    = UIImage(named: nope_image)
            
                
                break
                
            case .custom:
                custom_button.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
            
                custom_button.setImage(#imageLiteral(resourceName: "custom_icon_dark"), for: .normal)
                custom_button.setShadow(with_color: #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1), opacity: 0.15, radius: 20, offset_width: 0, offset_height: 6)
            
                game_mode_title.text   = custom_title
                game_mode_destail.text = custom_detail
                game_mode_img.image    = UIImage(named: custom_image)
                
                break
            
            case .silly:
                silly_button.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
            
                silly_button.setImage(#imageLiteral(resourceName: "silly_icon_dark"), for: .normal)
                silly_button.setShadow(with_color: #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1), opacity: 0.15, radius: 20, offset_width: 0, offset_height: 6)
            
                game_mode_title.text   = silly_title
                game_mode_destail.text = silly_detail
                game_mode_img.image    = UIImage(named: silly_image)
                
                break
            
            case .hot:
                hot_button.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
            
                hot_button.setImage(#imageLiteral(resourceName: "hot_icon_dark"), for: .normal)
                hot_button.setShadow(with_color: #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1), opacity: 0.15, radius: 20, offset_width: 0, offset_height: 6)
            
                game_mode_title.text   = hot_title
                game_mode_destail.text = hot_detail
                game_mode_img.image    = UIImage(named: hot_image)
            
                break
            
            case .springbreak:
                springbreak_button.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1)
            
                springbreak_button.setImage(#imageLiteral(resourceName: "springbreak_icon_dark"), for: .normal)
                springbreak_button.setShadow(with_color: #colorLiteral(red: 0.9450980392, green: 0.9176470588, blue: 0.8862745098, alpha: 1), opacity: 0.15, radius: 20, offset_width: 0, offset_height: 6)
            
                game_mode_title.text   = springbreak_title
                game_mode_destail.text = springbreak_detail
                game_mode_img.image    = UIImage(named: springbreak_image)
            
                break
        }
        
        transitionAnimation(on: pack)
        
    }
    
    
    
    //MARK: - Animations
    private func transitionAnimation(on views: [UIView]){
        let transition_animation = AnimationType.from(direction: .bottom, offset: 80)
        let zoom_animation       = AnimationType.zoom(scale: 0.2)
        
      
        animate(on: views, [zoom_animation, transition_animation], initia_alpha: 0, final_alpha: 1, duration: 0.5)
    }
    
    

}



