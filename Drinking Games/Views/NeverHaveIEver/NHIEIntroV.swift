//
//  NHIEIntroV.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imprt
import UIKit

class NHIEIntroV: UIView {

    //MARK: - Vars
    weak var vc: NHIEIntroVC?
    
    private let title_image: UIImageView = {
        let img = UIImageView()
        
        img.image = #imageLiteral(resourceName: "bear_img")
        img.contentMode = .scaleAspectFit
        
        return img
    }()
    private let title_label: UILabel     = {
        let lbl = UILabel()
        
        lbl.text          = "Never Have I Ever"
        lbl.font          = rubik.black.with(Size: 36)
        lbl.textAlignment = .center
        lbl.textColor     = #colorLiteral(red: 0.9254901961, green: 0.8509803922, blue: 0.7725490196, alpha: 1)
        
        lbl.setShadow(with_color: #colorLiteral(red: 0.9254901961, green: 0.8509803922, blue: 0.7725490196, alpha: 1), opacity: 0.3, radius: 6, offset_width: 0, offset_height: 3)
        
        return lbl
    }()
    
    private let player_table_container: UIView      = {
        let view = UIView()
        
        view.backgroundColor     = #colorLiteral(red: 0.2078431373, green: 0.168627451, blue: 0.1450980392, alpha: 1)
        view.layer.cornerRadius  = 14
        view.layer.masksToBounds = true
        
        return view
    }()
    private let player_table: UITableView           = {
        let tbl = UITableView()
        
        tbl.backgroundColor = #colorLiteral(red: 0.2078431373, green: 0.168627451, blue: 0.1450980392, alpha: 1)
        
        
        return tbl
    }() 
    
    private let start_container: RadialGradientView = {
        let view = RadialGradientView()
        
        view.inside_color  = #colorLiteral(red: 0.9294117647, green: 0.6862745098, blue: 0.262745098, alpha: 1)
        view.outside_color = #colorLiteral(red: 0.9215686275, green: 0.5529411765, blue: 0.1215686275, alpha: 1)
        
        view.layer.cornerRadius  = 30
        view.layer.masksToBounds = true
        
        return view
    }()
    private let start_button: UIButton              = {
        let btn             = UIButton()
        btn.backgroundColor = UIColor.clear
        
        btn.setTitle("Start", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        
        btn.titleLabel?.font = rubik.medium.with(Size: 20)
        
        return btn
    }()//TODO: - add targets!
    
    private let add_player_button: UIButton         = {
        let btn = UIButton()
        
        btn.setTitle("Add more player", for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0.8078431373, green: 0.662745098, blue: 0.5058823529, alpha: 1), for: .normal)
        
        btn.titleLabel?.font = rubik.medium.with(Size: 14)
        
        btn.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        
        return btn
    }()
    
    //MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        mainConfigurations()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LayOuting
    private func setupViews(){
        
        topQuarterViews()
        setupTopMiddleQuarterViews()
        setupBottomMiddleQuarterViews()
        setupBottomQuarterViews()
        
    }
    
    private func topQuarterViews(){
        addSubview(title_image)
        addSubview(title_label)
        
        title_image.anchorWithXCenter(top: self.safeAreaLayoutGuide.topAnchor, bottom: nil, top_constant: 15, bottom_constant: 0, width_constant: 185, height_constant: 175, center_x_with: self, x_constant: 0)
        
        title_label.anchorWithXCenter(top: self.title_image.bottomAnchor, bottom: nil, top_constant: 10, bottom_constant: 0, width_constant: 0, height_constant: 0, center_x_with: self, x_constant: 0)
    }
    
    private func setupTopMiddleQuarterViews(){
        addSubview(player_table_container)
        addSubview(player_table)
        
        player_table_container.anchorWithXCenter(top: title_label.bottomAnchor, bottom: nil, top_constant: 30, bottom_constant: 0, width_constant: 330, height_constant: 270, center_x_with: self, x_constant: 0)
        
        player_table.anchor(top: player_table_container.topAnchor, bottom: player_table_container.bottomAnchor, right: player_table_container.rightAnchor, left: player_table_container.leftAnchor, top_constant: 30, bottom_constant: 55, right_constant: 30, left_constant: 30, width_constant: 0, height_constant: 0)
        

    }
    
    private func setupBottomMiddleQuarterViews(){
        addSubview(add_player_button)
        
        add_player_button.anchor(top: player_table.bottomAnchor, bottom: player_table_container.bottomAnchor, right: player_table.rightAnchor, left: player_table.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
    }
    
    private func setupBottomQuarterViews(){
        addSubview(start_container)
        start_container.addSubview(start_button)
        
        start_container.anchorWithXCenter(top: nil, bottom: self.safeAreaLayoutGuide.bottomAnchor, top_constant: 0, bottom_constant: 15, width_constant: 300, height_constant: 60, center_x_with: self, x_constant: 0)
        
        start_button.anchor(top: start_container.topAnchor, bottom: start_container.bottomAnchor, right: start_container.rightAnchor, left: start_container.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
    }
    
    //MARK: - Configurations
    private func mainConfigurations(){
        self.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.1019607843, blue: 0.09803921569, alpha: 1)
        player_table.register(PlayerTVCell.self, forCellReuseIdentifier: player_table_cellid)
        addButtonToTarget()
        
    }
    
    func postConfigurations(){
        
        configuePlayerButtonUIEdgeInset()
        
    }
    
    private func addButtonToTarget(){
        
        add_player_button.addTarget(vc.self, action: #selector(vc?.addMoreCell(_:)), for: .touchUpInside)
        start_button.addTarget(vc.self, action: #selector(vc?.startButtonPressed(_:)), for: .touchUpInside)
    }
    
    func setPlayerTVDelegateDataSource(_ delegate: UITableViewDelegate, _ datasource: UITableViewDataSource){
        player_table.delegate   = delegate
        player_table.dataSource = datasource
    }
    
    private func configuePlayerButtonUIEdgeInset(){
        let image_width = add_player_button.imageView?.frame.width
        let btn_width   = add_player_button.frame.width
        
        add_player_button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: ((btn_width/2) - (image_width!/2)))
        
        add_player_button.titleEdgeInsets = UIEdgeInsets(top: 0, left: (btn_width/2) - image_width!, bottom: 0, right: -image_width!)
    }
    
    func playerTVReloadData(){
        player_table.reloadData()
    }
    
    func playerTVScrollBottom(){
      
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.player_table.numberOfRows(inSection:  0) - 1,
                section: self.player_table.numberOfSections - 1)
            self.player_table.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}
