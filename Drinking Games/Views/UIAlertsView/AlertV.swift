//
//  AlertV.swift
//  Drinking Games
//
//  Created by cain on 3/29/20.
//  Copyright © 2020 cain. All rights reserved.
//
//MARK: - Imports
import UIKit

class AlertV: UIView {

    //MARK: - Vars
    weak var vc: AlertVC?
    private var alert_container: UIView       = {
        var v = UIView()
        
        v.backgroundColor     = #colorLiteral(red: 0.1574268341, green: 0.1356399953, blue: 0.1297830641, alpha: 1)
        v.layer.cornerRadius  = 15
        v.layer.masksToBounds = true
        
        return v
    }()
    private var title_label: UILabel          = {
        var lbl = UILabel()
        
        lbl.numberOfLines = 1
        lbl.font          = rubik.medium.with(Size: 24)
        lbl.textColor     = .white
        lbl.textAlignment = .center
        
        return lbl
    }()
    private var body_label: UILabel           = {
        var lbl = UILabel()
        
        lbl.font          = rubik.regular.with(Size: 18)
        lbl.textColor     = .white
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        
        return lbl
    }()
    private let seprator: UIView              = {
        let v = UIView()
        
        v.backgroundColor = #colorLiteral(red: 0.4239223301, green: 0.4080207348, blue: 0.395337224, alpha: 1)
        
        return v
    }()
    private var action_button: UIButton       = {
        var btn = UIButton()
        
        btn.setTitle("OK", for: .normal)
        
        return btn
    }()
    private var cancel_button: UIButton       = {
        var btn = UIButton()
        
        return btn
    }()

    
    
    //Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        mainConfiguration()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //LayOuting
    private func setupView(){
        addSubview(alert_container)
        alert_container.addSubview(title_label)
        alert_container.addSubview(body_label)
        alert_container.addSubview(seprator)
        alert_container.addSubview(action_button)
        
        alert_container.anchorCenter(center_with: self, width: 300, height: 220)
        
        title_label.anchor(top: alert_container.topAnchor, bottom: nil, right: alert_container.rightAnchor, left: alert_container.leftAnchor, top_constant: 15, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
        
        body_label.anchor(top: title_label.bottomAnchor, bottom: alert_container.bottomAnchor, right: alert_container.rightAnchor, left: alert_container.leftAnchor, top_constant: 8, bottom_constant: 40, right_constant: 8, left_constant: 8, width_constant: 0, height_constant: 0)
        
        seprator.anchor(top: body_label.bottomAnchor, bottom: nil, right: alert_container.rightAnchor, left: alert_container.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0.75)
        
        action_button.anchor(top: seprator.bottomAnchor, bottom: alert_container.bottomAnchor, right: alert_container.rightAnchor, left: alert_container.leftAnchor, top_constant: 0, bottom_constant: 0, right_constant: 0, left_constant: 0, width_constant: 0, height_constant: 0)
        
        
    }
    
    
    //Configurations
    private func mainConfiguration(){
        
        self.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.595290493)
        
        addButtonsToTarget()
    }
    
    func postConfiguration(with tittle: String, message: String){
        
        self.title_label.text = tittle
        self.body_label.text  = message
        
    }
    
    private func addButtonsToTarget(){
        
        action_button.addTarget(vc.self, action: #selector(vc?.actionIsPressed(_:)), for: .touchUpInside)
        cancel_button.addTarget(vc.self, action: #selector(vc?.cancelIsPressed(_:)), for: .touchUpInside)
        
    }
    
    
    
    
    
}
