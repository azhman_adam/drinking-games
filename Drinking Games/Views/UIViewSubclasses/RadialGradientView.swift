//
//  RadialGradientView.swift
//  Drinking Games
//
//  Created by cain on 3/22/20.
//  Copyright © 2020 cain. All rights reserved.
//

import UIKit

class RadialGradientView: UIView {

    var inside_color: UIColor!
    var outside_color: UIColor!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        let colors     = [inside_color.cgColor, outside_color.cgColor] as CFArray
        let end_radius = min(frame.width, frame.height) / 2
        let gradient   = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        let center     = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        
        UIGraphicsGetCurrentContext()!.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: end_radius, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        
        
    }

}

